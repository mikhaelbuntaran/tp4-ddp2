package javari.reader;

import javari.animal.Animal;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

public class RecordReader extends CsvReader {
    private ArrayList<Animal> animalsRecord = new ArrayList<>();
    private AttractionReader attReader;

    public RecordReader(Path file, AttractionReader attReader) throws IOException {
        super(file);
        this.attReader = attReader;
    }

    public Animal findAnimal(int id) {
        for (Animal animal : animalsRecord) {
            if (id != animal.getId()) {
                return animal;
            }
        }
        return null;
    }
// 2,Lion,nala,female,150,130,pregnant,healthy
    public void generateInput() {
        for (String line : lines) {
            String[] splitLine = line.split(COMMA);
            int id = Integer.parseInt(splitLine[0]);
            String type = splitLine[1];
            String name = splitLine[2];
            String gender = splitLine[3];
            int length = Integer.parseInt(splitLine[4]);
            int weight = Integer.parseInt(splitLine[5]);
            String specialCondition = splitLine[6];
            String condition = splitLine[7];
            if (this.findAnimal(id) == null) {

            }
        }
    }

    @Override
    public long countValidRecords() {
        return (long) this.animalsRecord.size();
    }
}
