package javari.reader;

import javari.park.Attraction;
import javari.park.SelectedAttraction;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

public class AttractionReader extends CsvReader {
    private ArrayList<SelectedAttraction> attractionsList = new ArrayList<>();
    private RecordReader record;

    public AttractionReader(Path file, RecordReader record) throws IOException {
        super(file);
        this.record = record;
    }

    public SelectedAttraction findAttraction(String name, String type) {
        for (SelectedAttraction attraction : attractionsList) {
            if (attraction.getName().equals(name) && attraction.getType().equals(type)) {
                return attraction;
            }
        }
        return null;
    }
// Whale,Circles of Fires
    public void generateInput() {
        for (String line : lines) {
            String[] splitLines = line.split(COMMA);
            String type = splitLines[0];
            String name = splitLines[1];
            SelectedAttraction attraction = this.findAttraction(name, type);
            if (attraction == null) {
                this.attractionsList.add(new Attraction(name, type));
            }
        }
    }

    @Override
    public long countValidRecords() {
        return (long) this.attractionsList.size();
    }
}
