package javari.reader;

import javari.park.Section;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

public class SectionReader extends CsvReader {
    private ArrayList<Section> animalsSection = new ArrayList<>();

    public SectionReader(Path file) throws IOException {
        super(file);
    }

    public Section findSection(String name) {
        for (Section section : animalsSection) {
            if (section.getName().equals(name)) {
                return section;
            }
        }
        return null;
    }

    // Hamster,mammals,Explore the Mammals
    public void generateInput() {
        for (String line : lines) {
            String[] splitLines = line.split(COMMA);
            String type = splitLines[0];
            String category = splitLines[1];
            String section = splitLines[2];
            if (this.findSection(section) == null) {
                this.animalsSection.add(new Section(section, category));
            }
            this.findSection(category).getTypes().add(type);
        }
    }

    @Override
    public long countValidRecords() {
        return (long) (this.animalsSection.size());
    }
}
