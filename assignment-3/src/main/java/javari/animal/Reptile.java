package javari.animal;

/**
 * This class represents attributes and behaviours found in all reptiles in Javari Park.
 *
 * @author Mikhael (NPM 1706039811)
 *
 */
public class Reptile extends Animal {
    protected boolean isTamed;

    public Reptile(Integer id, String type, String name, Gender gender, double length,
                   double weight, String tame, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.isTamed = tame.equals("tame");
    }

    @Override
    public boolean specificCondition() {
        return this.isTamed;
    }
}
