package javari.animal;

/**
 * This class represents attributes and behaviours found in all mammals in Javari Park.
 *
 * @author Mikhael (NPM 1706039811)
 *
 */
public class Mammal extends Animal {
    protected boolean isPregnant;

    public Mammal(Integer id, String type, String name, Gender gender, double length,
                  double weight, String pregnant, Condition condition) {
        super(id, type, name,gender,length, weight, condition);
        this.isPregnant = pregnant.equals("pregnant");
    }

    @Override
    public boolean specificCondition() {
        return !this.isPregnant &&
                !(this.getType().equals("Lion") && this.getGender().equals(Gender.FEMALE));
    }
}
