package javari.animal;

/**
 * This class represents attributes and behaviours found in all aves in Javari Park.
 *
 * @author Mikhael (NPM 1706039811)
 *
 */
public class Aves extends Animal {
    protected boolean isLayingEggs;

    public Aves(Integer id, String type, String name, Gender gender, double length,
                double weight, String layingEggs, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.isLayingEggs = layingEggs.equals("laying eggs");
    }

    @Override
    public boolean specificCondition() {
        return !this.isLayingEggs;
    }
}
