package javari.park;

import java.util.ArrayList;
import java.util.List;

public class Visitor implements Registration {
    private String name;
    private int id;
    private List<SelectedAttraction> attractions = new ArrayList<>();

    public Visitor(String name, int id) {
        this.name = name;
        this.id = id;
    }

    @Override
    public String getVisitorName() {
        return this.name;
    }

    @Override
    public void setVisitorName(String name) {
        this.name = name;
    }

    @Override
    public int getRegistrationId() {
        return this.id;
    }

    @Override
    public List<SelectedAttraction> getSelectedAttractions() {
        return this.attractions;
    }

    @Override
    public boolean addSelectedAttraction(SelectedAttraction selected) {
        if (selected == null) {
            return false;
        }
        attractions.add(selected);
        return true;
    }
}
