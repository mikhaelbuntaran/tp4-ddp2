package javari.park;

import java.util.ArrayList;

public class Section {
    private String name;
    private String category;
    private ArrayList<String> types = new ArrayList<>();

    public Section(String name, String category){
        this.name = name;
        this.category = category;
    }

    public String getName() {
        return this.name;
    }

    public String getCategory() {
        return this.category;
    }

    public ArrayList<String> getTypes() {
        return this.types;
    }
}
