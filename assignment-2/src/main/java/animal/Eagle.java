package animal;

public class Eagle extends Animal {

    private static final String FLYING_SOUND = "kwaakk...";

    public Eagle(String name, int length) {
        super(name, length, true);
    }

    public void fly() {
        System.out.println(FLYING_SOUND);
    }
}