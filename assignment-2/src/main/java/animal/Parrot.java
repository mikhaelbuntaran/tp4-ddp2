package animal;

public class Parrot extends Animal {

    private static final String FLYING_SOUND = "FLYYYY...";
    private static final String MIMIC_NULL_SOUND = "HM?";

    public Parrot(String name, int length) {
        super(name, length, false);
    }

    public void fly() {
        System.out.println(FLYING_SOUND);
    }

    public void mimic() {
        System.out.println(MIMIC_NULL_SOUND);
    }

    public void mimic(String words) {
        System.out.println(words.toUpperCase());
    }
}