package animal;

public class Hamster extends Animal {

    private static final String GNAWING_SOUND = "Ngkkrit.. Ngkrrriiit";
    private static final String RUNNING_SOUND = "Trrr... Trrr...";

    public Hamster(String name, int length) {
        super(name, length, false);
    }

    public void gnaw() {
        System.out.println(GNAWING_SOUND);
    }

    public void run() {
        System.out.println(RUNNING_SOUND);
    }
}