package animal;

public class Lion extends Animal {

    private static final String HUNTING_SOUND = "err...!";
    private static final String BRUSHING_SOUND = "Hauhhmm!";
    private static final String DISTURBED_SOUND = "HAUHHMM!";

    public Lion(String name, int length) {
        super(name, length, true);
    }

    public void hunt() {
        System.out.println(HUNTING_SOUND);
    }

    public void brushed() {
        System.out.println(BRUSHING_SOUND);
    }

    public void disturbed() {
        System.out.println(DISTURBED_SOUND);
    }
}