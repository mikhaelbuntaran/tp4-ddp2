package animal;
import cage.Cage;

public class Animal {

    protected String name;
    protected int length;
    protected boolean isWild;
    protected Cage cage;

    public Animal(String name, int length, boolean isWild) {
        this.name = name;
        this.length = length;
        this.isWild = isWild;
        if (!isWild) {
            if (length > 60) {
                this.cage = new Cage(this, true, "C");
            } else if (45 <= length && length <= 60) {
                this.cage = new Cage(this, true, "B");
            } else {
                this.cage = new Cage(this, true, "A");
            }
        } else {
            if (length > 90) {
                this.cage = new Cage(this, false, "C");
            } else if (75 <= length && length <= 90) {
                this.cage = new Cage(this, false, "B");
            } else {
                this.cage = new Cage(this, false, "A");
            }
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public int getLength() {
        return this.length;
    }

    public boolean isWild() {
        return this.isWild;
    }

    public Cage getCage() {
        return this.cage;
    }

}