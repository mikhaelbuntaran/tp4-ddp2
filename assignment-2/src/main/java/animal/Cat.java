package animal;
import java.util.Random;


public class Cat extends Animal {

    private static final String BRUSHING_SOUND = "Nyaaan...";
    private static final String[] CUDDLING_SOUNDS = {"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};
    private static Random random = new Random();

    public Cat(String name, int length) {
        super(name, length, false);
    }

    public void brushed() {
        System.out.println(BRUSHING_SOUND);
    }

    public void cuddled() {
        System.out.println(CUDDLING_SOUNDS[random.nextInt(CUDDLING_SOUNDS.length)]);
    }
}