import animal.*;
import cage.Levels;

import java.util.Scanner;
import java.util.ArrayList;

public class JavariPark {
    
    static String[] animals = {"cat", "lion", "eagle", "parrot", "hamster"};
    static ArrayList<ArrayList<Animal>> cats = new ArrayList<ArrayList<Animal>>();
    static ArrayList<ArrayList<Animal>> lions = new ArrayList<ArrayList<Animal>>();
    static ArrayList<ArrayList<Animal>> eagles = new ArrayList<ArrayList<Animal>>();
    static ArrayList<ArrayList<Animal>> parrots = new ArrayList<ArrayList<Animal>>();
    static ArrayList<ArrayList<Animal>> hamsters = new ArrayList<ArrayList<Animal>>();
    static int totalCats;
    static int totalLions;
    static int totalEagles;
    static int totalParrots;
    static int totalHamsters;

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Welcome to Javari Park!");
        System.out.println("Input the number of animals");
        for (int a = 0; a < animals.length; a++) {
            System.out.printf("%s: ", animals[a]);
            String total = input.nextLine();
            int totalAnimals = Integer.parseInt(total);
            if (animals[a].equals("cat")) {
                totalCats = totalAnimals;
            } else if (animals[a].equals("lion")) {
                totalLions = totalAnimals;
            } else if (animals[a].equals("eagle")) {
                totalEagles = totalAnimals;
            } else if (animals[a].equals("parrot")) {
                totalParrots = totalAnimals;
            } else if (animals[a].equals("hamster")){
                totalHamsters = totalAnimals;
            }
            if (totalAnimals <= 0) {
                continue;
            }
            System.out.printf("Provide the information of %s(s):%n", animals[a]);
            String inputAnimal = input.nextLine();
            String[] splitAnimals = inputAnimal.split(",");
            String[][] animalsTemp = new String[totalAnimals][2];
            for (int i = 0; i < totalAnimals; i++) {
                animalsTemp[i] = splitAnimals[i].split("\\|");
            }
            Animal[] animalsList = new Animal[totalAnimals];
            for (int i = 0; i < totalAnimals; i++) {
                int length = Integer.parseInt(animalsTemp[i][1]);
                Animal animal;
                if (animals[a].equals("cat")) {
                    animal = new Cat(animalsTemp[i][0], length);
                    animalsList[i] = animal;
                } else if (animals[a].equals("lion")) {
                    animal = new Lion(animalsTemp[i][0], length);
                    animalsList[i] = animal;
                } else if (animals[a].equals("eagle")) {
                    animal = new Eagle(animalsTemp[i][0], length);
                    animalsList[i] = animal;
                } else if (animals[a].equals("parrot")) {
                	animal = new Parrot(animalsTemp[i][0], length);
                    animalsList[i] = animal;
                } else if (animals[a].equals("hamster")) {
                    animal = new Hamster(animalsTemp[i][0], length);
                    animalsList[i] = animal;
                }
            }
            ArrayList<ArrayList<Animal>> animalLevels = new ArrayList<ArrayList<Animal>>();
            for (int x = 0; x < 3; x++) {
                animalLevels.add(new ArrayList<Animal>());
            }
            int lvl = 0;
            int len = totalAnimals / 3;
            if (animals[a].equals("cat")) {
                for (int i = 0; i < totalAnimals; i++) {
                    if (i != 0 && i % len == 0 && lvl < 2) {
                        lvl++;
                    }
                    animalLevels.get(lvl).add(animalsList[i]);
                }
                cats = animalLevels;
            } else if (animals[a].equals("lion")) {
                for (int i = 0; i < totalAnimals; i++) {
                    if (i != 0 && i % len == 0 && lvl < 2) {
                        lvl++;
                    }
                    animalLevels.get(lvl).add(animalsList[i]);
                }
                lions = animalLevels;
            } else if (animals[a].equals("eagle")) {
                for (int i = 0; i < totalAnimals; i++) {
                    if (i != 0 && i % len == 0 && lvl < 2) {
                        lvl++;
                    }
                    animalLevels.get(lvl).add(animalsList[i]);
                }
                eagles = animalLevels;
            } else if (animals[a].equals("parrot")) {
                for (int i = 0; i < totalAnimals; i++) {
                    if (i != 0 && i % len == 0 && lvl < 2) {
                        lvl++;
                    }
                    animalLevels.get(lvl).add(animalsList[i]);
                }
                parrots = animalLevels;
            } else if (animals[a].equals("hamster")) {
                for (int i = 0; i < totalAnimals; i++) {
                    if (i != 0 && i % len == 0 && lvl < 2) {
                        lvl++;
                    }
                    animalLevels.get(lvl).add(animalsList[i]);
                }
                hamsters = animalLevels;
            }
        }
        System.out.println("Animals have been successfully recorded!\n");
        System.out.println("=============================================");
        System.out.println("Cage arrangement:");
        for (int i = 0; i < animals.length; i++) {
            if (animals[i].equals("cat") && cats.size() > 1) {
                System.out.println("location: indoor");
                Levels.printLevels(cats);
                cats = Levels.arrange(cats);
                System.out.println("\nAfter rearrangement...");
                Levels.printLevels(cats);
                System.out.println();
            } else if (animals[i].equals("lion") && lions.size() > 1) {
                System.out.println("location: outdoor");
                Levels.printLevels(lions);
                lions = Levels.arrange(lions);
                System.out.println("\nAfter rearrangement...");
                Levels.printLevels(lions);
                System.out.println();
            } else if (animals[i].equals("eagle") && eagles.size() > 1) {
                System.out.println("location: outdoor");
                Levels.printLevels(eagles);
                eagles = Levels.arrange(eagles);
                System.out.println("\nAfter rearrangement...");
                Levels.printLevels(eagles);
                System.out.println();
            } else if (animals[i].equals("parrot") && parrots.size() > 1) {
                System.out.println("location: indoor");
                Levels.printLevels(parrots);
                parrots = Levels.arrange(parrots);
                System.out.println("\nAfter rearrangement...");
                Levels.printLevels(parrots);
                System.out.println();
            } else if (animals[i].equals("hamster") && hamsters.size() > 1) {
                System.out.println("location: indoor");
                Levels.printLevels(hamsters);
                hamsters = Levels.arrange(hamsters);
                System.out.println("\nAfter rearrangement...");
                Levels.printLevels(hamsters);
                System.out.println();
            }
        }

        System.out.println("NUMBER OF ANIMALS:");
        for (int a = 0; a < animals.length; a++) {
            if (animals[a].equals("cat")) {
                System.out.printf("%s:%d%n", animals[a], totalCats);
            } else if (animals[a].equals("lion")) {
                System.out.printf("%s:%d%n", animals[a], totalLions);
            } else if (animals[a].equals("eagle")) {
                System.out.printf("%s:%d%n", animals[a], totalEagles);
            } else if (animals[a].equals("parrot")) {
                System.out.printf("%s:%d%n", animals[a], totalParrots);
            } else if (animals[a].equals("hamster")) {
                System.out.printf("%s:%d%n", animals[a], totalHamsters);
            }
        }

        System.out.println("Animals have been successfully recorded!\n");
        System.out.println("=============================================");
        boolean valid = true;
        boolean exist = true;
        String action;
        Animal ani = null;
        while(valid) {
            System.out.println("Which animal you want to visit?");
            System.out.println("(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");
            String choice = input.nextLine();
            if (choice.equals("1")) {
                System.out.print("Mention the name of cat you want to visit: ");
                String name = input.nextLine();
                for (ArrayList<Animal> level : cats) {
                    for (Animal cat : level) {
                        if (cat.getName().equals(name)) {
                            exist = true;
                            ani = cat;
                        } else {
                            exist = false;
                        }
                    }
                }
                if (exist) {
                    System.out.printf("You are visiting %s (cat) now, " +
                            "what would you like to do?%n", ani.getName());
                    System.out.println("1: Brush the fur 2: Cuddle");
                    action = input.nextLine();
                    if (action.equals("1")) {
                        System.out.printf("Time to clean %s's fur%n", ani.getName());
                        System.out.printf("%s makes a voice: ", ani.getName());
                        ((Cat) ani).brushed();
                        System.out.println("Back to the office!\n");
                    } else if (action.equals("2")) {
                        System.out.printf("%s makes a voice: ", ani.getName());
                        ((Cat) ani).cuddled();
                        System.out.println("Back to the office!\n");
                    } else {
                        System.out.println("You do nothing...");
                        System.out.println("Back to the office!\n");
                    }
                }  else {
                    System.out.println("There is no cat with that name!" +
                            " Back to the office!\n");
                    exist = true;
                }
            } else if (choice.equals("2")) {
                System.out.print("Mention the name of eagle you want to visit: ");
                String name = input.nextLine();
                for (ArrayList<Animal> level : eagles) {
                    for (Animal eagle : level) {
                        if (eagle.getName().equals(name)) {
                            exist = true;
                            ani = eagle;
                        } else {
                            exist = false;
                        }
                    }
                }
                if (exist) {
                    System.out.printf("You are visiting %s (eagle) now, " +
                            "what would you like to do?%n", ani.getName());
                    System.out.println("1: Order to fly");
                    action = input.nextLine();
                    if (action.equals("1")) {
                        System.out.printf("%s makes a voice: ", ani.getName());
                        ((Eagle) ani).fly();
                        System.out.println("You hurt!");
                        System.out.println("Back to the office!\n");
                    } else {
                        System.out.println("You do nothing...");
                        System.out.println("Back to the office!\n");
                    }
                }  else {
                    System.out.println("There is no eagle with that name! " +
                            "Back to the office!\n");
                    exist = true;
                }
            } else if (choice.equals("3")) {
                System.out.print("Mention the name of hamster you want to visit: ");
                String name = input.nextLine();
                for (ArrayList<Animal> level : hamsters) {
                    for (Animal hamster : level) {
                        if (hamster.getName().equals(name)) {
                            exist = true;
                            ani = hamster;
                        } else {
                            exist = false;
                        }
                    }
                }
                if (exist) {
                    System.out.printf("You are visiting %s (hamster) now, " +
                            "what would you like to do?%n", ani.getName());
                    System.out.println("1: See it gnawing 2: Order to run in the hamster wheel");
                    action = input.nextLine();
                    if (action.equals("1")) {
                        System.out.printf("%s makes a voice: ", ani.getName());
                        ((Hamster) ani).gnaw();
                        System.out.println("Back to the office!\n");
                    } else if (action.equals("2")) {
                        System.out.printf("%s makes a voice: ", ani.getName());
                        ((Hamster) ani).run();
                        System.out.println("Back to the office!\n");
                    } else {
                        System.out.println("You do nothing...");
                        System.out.println("Back to the office!\n");
                    }
                } else {
                    System.out.println("There is no hamster with that name!" +
                            " Back to the office!\n");
                    exist = true;
                }
            } else if (choice.equals("4")) {
                System.out.print("Mention the name of parrot you want to visit: ");
                String name = input.nextLine();
                for (ArrayList<Animal> level : parrots) {
                    for (Animal parrot : level) {
                        if (parrot.getName().equals(name)) {
                            exist = true;
                            ani = parrot;
                        } else {
                            exist = false;
                        }
                    }
                }
                if (exist) {
                    System.out.printf("You are visiting %s (parrot) now, " +
                            "what would you like to do?%n", ani.getName());
                    System.out.println("1: Order to fly 2: Do conversation");
                    action = input.nextLine();
                    if (action.equals("1")) {
                        System.out.printf("Parrot %s flies!%n", ani.getName());
                        System.out.printf("%s makes a voice: ", ani.getName());
                        ((Parrot) ani).fly();
                        System.out.println("Back to the office!\n");
                    } else if (action.equals("2")) {
                        System.out.print("You say: ");
                        String words = input.nextLine();
                        System.out.printf("%s says: ", ani.getName());
                        ((Parrot) ani).mimic(words);
                        System.out.println("Back to the office!\n");
                    } else {
                        System.out.println("You do nothing...");
                        System.out.printf("%s says: ", ani.getName());
                        ((Parrot) ani).mimic();
                        System.out.println("Back to the office!\n");
                    }
                } else {
                    System.out.println("There is no parrot with that name!" +
                            " Back to the office!\n");
                    exist = true;
                }
            } else if (choice.equals("5")) {
                System.out.print("Mention the name of lion you want to visit: ");
                String name = input.nextLine();
                for (ArrayList<Animal> level : lions) {
                    for (Animal lion : level) {
                        if (lion.getName().equals(name)) {
                            exist = true;
                            ani = lion;
                        } else {
                            exist = false;
                        }
                    }
                }
                if (exist) {
                    System.out.printf("You are visiting %s (lion) now, " +
                            "what would you like to do?%n", ani.getName());
                    System.out.println("1: See it hunting 2: Brush the mane 3: Disturb it");
                    action = input.nextLine();
                    if (action.equals("1")) {
                        System.out.println("Lion is hunting..");
                        System.out.printf("%s makes a voice: ", ani.getName());
                        ((Lion) ani).hunt();
                        System.out.println("Back to the office!\n");
                    } else if (action.equals("2")) {
                        System.out.println("Clean the lion's mane..");
                        System.out.printf("%s makes a voice: ", ani.getName());
                        ((Lion) ani).brushed();
                        System.out.println("Back to the office!\n");
                    } else if (action.equals("3")) {
                        System.out.printf("%s makes a voice: ", ani.getName());
                        ((Lion) ani).disturbed();
                        System.out.println("Back to the office!\n");
                    } else {
                        System.out.println("You do nothing...");
                        System.out.printf("%s says: ", ani.getName());
                        ((Parrot) ani).mimic();
                        System.out.println("Back to the office!\n");
                    }
                } else {
                    System.out.println("There is no lion with that name! " +
                            "Back to the office!\n");
                    exist = true;
                }
            } else if (choice.equals("99")) {
                valid = false;
            }
        }
    }
}