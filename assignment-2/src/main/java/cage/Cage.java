package cage;

import animal.Animal;

public class Cage {
    
    private static final int WIDTH_OUTDOOR = 120;
    private static final int LENGTH_SMALL_OUTDOOR = WIDTH_OUTDOOR;
    private static final int LENGTH_MEDIUM_OUTDOOR = 150;
    private static final int LENGTH_LARGE_OUTDOOR = 180;

    private static final int WIDTH_INDOOR = 60;
    private static final int LENGTH_SMALL_INDOOR = WIDTH_INDOOR;
    private static final int LENGTH_MEDIUM_INDOOR = 90;
    private static final int LENGTH_LARGE_INDOOR = WIDTH_OUTDOOR;

    private Animal inhabitant;
    private boolean isIndoor;
    private String type;
    private int length;
    private int width;

    public Cage(Animal inhabitant, boolean isIndoor, String type) {
        this.inhabitant = inhabitant;
        this.isIndoor = isIndoor;
        this.type = type;
        if (isIndoor) {
            this.width = WIDTH_INDOOR;
            if (type.equals("A")) {
                this.length = LENGTH_SMALL_INDOOR;
            } else if (type.equals("B")) {
                this.length = LENGTH_MEDIUM_INDOOR;
            } else {
                this.length = LENGTH_LARGE_INDOOR;
            }
        } else {
            this.width = WIDTH_OUTDOOR;
            if (type.equals("A")) {
                this.length = LENGTH_SMALL_OUTDOOR;
            } else if (type.equals("B")) {
                this.length = LENGTH_MEDIUM_OUTDOOR;
            } else {
                this.length = LENGTH_LARGE_OUTDOOR;
            }
        }
    }

    public void setInhabitant(Animal inhabitant) {
        this.inhabitant = inhabitant;
    }

    public Animal getInhabitant() {
        return this.inhabitant;
    }

    public void setIndoor(boolean isIndoor) {
        this.isIndoor = isIndoor;
    }

    public boolean isIndoor() {
        return this.isIndoor;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return this.type; 
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getLength() {
        return this.length;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getWidth() {
        return this.width;
    }
}