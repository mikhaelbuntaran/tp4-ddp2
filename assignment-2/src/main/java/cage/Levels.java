package cage;
import animal.*;

import java.util.ArrayList;

public class Levels {
    public static ArrayList<ArrayList<Animal>> arrange(ArrayList<ArrayList<Animal>> levels) {
        ArrayList<Animal> temp = new ArrayList<Animal>(levels.get(2));
        levels.set(2, new ArrayList<Animal>(levels.get(1)));
        levels.set(1, new ArrayList<Animal>(levels.get(0)));
        levels.set(0, new ArrayList<Animal>(temp));
        for (int i = 0; i < levels.size(); i++) {
            for (int j = 0; j < levels.get(i).size()/2; j++) {
                Animal tmp = levels.get(i).get(j);
                levels.get(i).set(j, levels.get(i).get(levels.get(i).size()-j-1));
                levels.get(i).set(levels.get(i).size()-j-1, tmp);
            }
        }
        return levels;
    }

    public static void printLevels(ArrayList<ArrayList<Animal>> levels) {
        String animalName;
        int animalLength;
        String cageType;
        for (int i = levels.size(); i > 0; i--) {
            System.out.printf("level %d: ", i);
            for (int j = 0; j < levels.get(i-1).size(); j++) {
                animalName = levels.get(i-1).get(j).getName();
                animalLength = levels.get(i-1).get(j).getLength();
                cageType = levels.get(i-1).get(j).getCage().getType();
                System.out.printf("%s (%d - %s), ", animalName, animalLength, cageType);
            }
            System.out.println();
        }
    }
}