import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Class representing the board and menu of match-pair memory game.
 * @author Mikhael (NPM: 1706039811)
 * @version 17.05.2018
 */
public class Board extends JFrame {
    private ArrayList<Card> cards = new ArrayList<>();
    private static ImageIcon backCard = new ImageIcon("img/ClosedCard.png");

    private static final int MAX_SEELECTED_CARDS = 2;

    private Card currentCard;
    private Card previousCard;
    private int selectedCards = 0;

    private int countAttempt = 0;

    /**
     * Constructor for board class.
     */
    public Board() {
        // constructs the frame
        super("Match-pair Memory Game");
        setBackground(Color.WHITE);
        setLayout(new BorderLayout());

        // makes label containing information of number of attempts
        JLabel attemptLabel = new JLabel("Number of attempts: " + countAttempt);

        // sets panel for the cards
        JPanel cardPanel = new JPanel();
        cardPanel.setLayout(new GridLayout(6, 6));

        // adds face of the card to list of face of the card and shuffles it
        ArrayList<String> faceCardStrList = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            faceCardStrList.add("img/AgathaChelsea.png");
            faceCardStrList.add("img/AmandaCerny.png");
            faceCardStrList.add("img/ChelseaIslan.png");
            faceCardStrList.add("img/ChloeBennet.png");
            faceCardStrList.add("img/DianSastro.png");
            faceCardStrList.add("img/ElizabethOlsen.png");
            faceCardStrList.add("img/EllenPage.png");
            faceCardStrList.add("img/GalGadot.png");
            faceCardStrList.add("img/HaileeSteinfield.png");
            faceCardStrList.add("img/ScarlettJohansson.png");
            faceCardStrList.add("img/VaneshaPreschilla.png");
        }
        Collections.shuffle(faceCardStrList);

        // sets the cards
        for (String str : faceCardStrList) {
            Card card = new Card(str);
            cards.add(card);
            card.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Card clickedCard = (Card) e.getSource();
                    // if card is closed
                    if (!clickedCard.isOpened() && selectedCards < MAX_SEELECTED_CARDS) {
                        if (currentCard == null) {
                            currentCard = clickedCard;
                            currentCard.openCard();
                            selectedCards++;
                        } else if (previousCard == null) {
                            previousCard = clickedCard;
                            previousCard.openCard();
                            selectedCards++;
                        }
                    }
                    try {
                        if (currentCard.isOpened() && previousCard.isOpened() &&
                                currentCard != null && previousCard != null &&
                                selectedCards == MAX_SEELECTED_CARDS) {
                            Timer timer = new Timer(500, new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent e) {
                                    try {
                                        if (currentCard.getFaceStr().equals(
                                                previousCard.getFaceStr())) {
                                            previousCard.setMatched(true);
                                            previousCard.setMatched(true);
                                            currentCard.setEnabled(false);
                                            previousCard.setEnabled(false);
                                            currentCard.setVisible(false);
                                            previousCard.setVisible(false);
                                        } else {
                                            currentCard.closeCard();
                                            previousCard.closeCard();
                                        }
                                        currentCard = null;
                                        previousCard = null;
                                        selectedCards = 0;
                                        attemptLabel.setText("Number of attempts: " + ++countAttempt);
                                    } catch (NullPointerException ex) { }
                                }
                            });
                            timer.start();
                            timer.setRepeats(false);
                        }
                    } catch (NullPointerException ex) { }
                }
            });
            cardPanel.add(card);
        }

        // places deck panel on board
        add(cardPanel, BorderLayout.CENTER);

        // set panel for the menu
        JPanel menuPanel = new JPanel();
        menuPanel.setLayout(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints();

        // makes play again button (pressing it will reset the game)
        JButton playAgainButton = new JButton("Start Over");
        playAgainButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                countAttempt = 0;
                selectedCards = 0;
                attemptLabel.setText("Number of attempts: " + countAttempt);
                Collections.shuffle(cards);
                cardPanel.removeAll();
                currentCard = null;
                previousCard = null;
                for (Card card : cards) {
                    card.setEnabled(true);
                    card.setVisible(true);
                    card.setOpened(false);
                    card.setMatched(false);
                    card.setIcon(backCard);
                    cardPanel.add(card);
                }
            }
        });

        // places play again button to menu panel
        gc.gridx = 0;
        gc.gridy = 0;
        menuPanel.add(playAgainButton, gc);

        // makes exit button (press it to exit the game)
        JButton exitButton = new JButton("Exit");
        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        // places play again button to menu panel
        gc.gridx = 1;
        gc.gridy = 0;
        menuPanel.add(exitButton, gc);

        //places
        gc.gridx = 0;
        gc.gridy = 1;
        menuPanel.add(attemptLabel, gc);
        add(menuPanel, BorderLayout.SOUTH);
    }
}