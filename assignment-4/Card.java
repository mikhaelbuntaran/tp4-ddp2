import javax.swing.*;

/**
 * Class representing the card of match-pair memory game.
 * @author Mikhael (NPM: 1706039811)
 * @version 17.05.2018
 */
public class Card extends JButton {
    private ImageIcon face;
    private String faceStr;
    private static ImageIcon back = new ImageIcon("img/ClosedCard.png");
    private boolean isOpened = false;
    private boolean isMatched = false;

    /**
     * Constructor of class Card.
     * @param faceStr image of the card's face
     */
    public Card(String faceStr) {
        super(back);
        setSize(150,150);
        this.faceStr = faceStr;
        this.face = new ImageIcon(faceStr);
    }

    /**
     * Accessor for the image of the card when it is opened.
     * @return image of the card when it is opened
     */
    public ImageIcon getFace() {
        return face;
    }

    /**
     * Accessor for the filename of the image of the card when it is opened.
     * @return filename of the image of the card when it is opened
     */
    public String getFaceStr() {
        return faceStr;
    }

    /**
     * Accessor for the condition of the card, whether it is opened or closed.
     * @return true if the card is open, and false if otherwise
     */
    public boolean isOpened() {
        return isOpened;
    }

    /**
     * Mutator for the condition of the card, whether it is opened or closed.
     * @param opened true if the card is open, and false if otherwise
     */
    public void setOpened(boolean opened) {
        isOpened = opened;
    }

    /**
     * Mutator for the condition of the card, whether it has been matched or not.
     * @param matched true if the card has been matched, and false if otherwise
     */
    public void setMatched(boolean matched) {
        isMatched = matched;
    }

    /**
     * Opens the card. This method will change the Icon of the card to the face of the card.
     */
    public void openCard() {
        setIcon(face);
        isOpened = true;
    }

    /**
     * Closes the card. This method will change the Icon of the card to the back of the card.
     */
    public void closeCard() {
        setIcon(back);
        isOpened = false;
    }
}