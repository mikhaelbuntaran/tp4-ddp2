import javax.swing.*;

/**
 * @author Mikhael (NPM 1706039811)
 * @version 17.05.2018
 */
public class MemoryGameApp {
    public static void main(String args[]) {
        JFrame board = new Board();
        board.setSize(900, 950);
        board.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        board.setVisible(true);
    }
}