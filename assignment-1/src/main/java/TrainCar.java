public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms

    WildCat cat;
    TrainCar next;  // to keep the train car next to this train car

    // constructor 1 (if train car has no other train car next to it)
    public TrainCar(WildCat cat) {
        this.cat = cat;
    }

    // constructor 2 (if train car has another train car next to it)
    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.next = next;
    }

    // method to compute the train's weight
    public double computeTotalWeight() {
        if (next == null) {   //base case
            return cat.weight + EMPTY_WEIGHT;
        }
        // recursive case
        return cat.weight + EMPTY_WEIGHT + next.computeTotalWeight();
    }

    // method to compute total mass index of the cats in the train
    public double computeTotalMassIndex() {
        if (next == null) {   //base case
            return cat.computeMassIndex();
        }
        // recursive case
        return cat.computeMassIndex() + next.computeTotalMassIndex();
    }

    // method to print the train
    public void printCar() {
        if (next == null) {   // base case
            System.out.println("(" + cat.name + ")");
        } else {                        // recursive case
            System.out.print("(" + cat.name + ")--");
            next.printCar();
        }
    }
}
