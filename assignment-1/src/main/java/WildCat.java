public class WildCat {

    String name;
    double weight; // In kilograms
    double length; // In centimeters

    // constructor
    public WildCat(String name, double weight, double length) {
        this.name = name;
        this.weight = weight;
        this.length = length;
    }

    // method to compute the cat's mass index (weight / length^2 (in meters))
    public double computeMassIndex() {
        return weight * 10000 / (length * length);
    } 
}