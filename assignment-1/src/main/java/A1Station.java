import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms

    static TrainCar train;
    static int catsInTrain = 0; // counter to count the cat in the train

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String numberOfCats = input.nextLine();
        int numberOfCatsInt = Integer.parseInt(numberOfCats);
        for (int i = 0; i < numberOfCatsInt; i++) {
            String cat = input.nextLine();
            String[] catSplitted = cat.split(",");
            double w = Double.parseDouble(catSplitted[1]);
            double h = Double.parseDouble(catSplitted[2]);
            WildCat javariCat = new WildCat(catSplitted[0], w, h);
            catsInTrain++;
            if (train == null) {
                train = new TrainCar(javariCat);
            } else {
                train = new TrainCar(javariCat, train);
            }
            if (train.computeTotalWeight() > THRESHOLD) {
                departure();
                catsInTrain = 0;
                train = null;
            }
        }
        if (train != null) {
            departure();
        }
        
        input.close();
    }

    // method to print departure message
    public static void departure() {
        System.out.println("The train departs to Javari Park");
        System.out.print("[LOCO]<--");
        train.printCar();
        double aveMassIndex = average(train.computeTotalMassIndex(),catsInTrain);
        System.out.format("Average mass index of all cats: %.2f\n",aveMassIndex);
        System.out.println("In average, the cats in the train are " + checkCategory(aveMassIndex));
    }

    // method to check cat's category based on its mass index
    public static String checkCategory(double massIndex) {
        if (massIndex < 18.5) {
            return "underweight";
        } else if (massIndex < 25) {
            return "normal";
        } else if (massIndex < 30) {
            return "overweight";
        }
        return "obese";
    }

    // method to count average
    public static double average(double total, int frequency) {
        return total / frequency;
    }
}
